#How to Run
1. Install angular-cli
`npm install -g @angular/cli`

2. Navigate to client folder
`cd client`

3. Use angular cli to build and run the project
`ng serve`

# Front End Coding Challenge

## What is this repository for?
This repository should be used to push your solution for the Morressier front-end coding challenge. 

## How do I get set up?
We have build a small backend to which you should build a frontend. The documentation of the API can be found [here](http://mcc17.herokuapp.com/)
Please commit your code constantly into the repository. Just work as you would normally do it and once you’ve completed the task, just let us know. We will be reviewing your solution and will invite you for a personal meeting in our office. 

## Background information
We are transforming the academic industry into digital by converting traditional paper posters into e-posters. These e-posters will be presented on major international conferences with our iPad App (connected to a screen or projector). In order to manage these eposters, we’ve crafted www.morressier.com. 

## Task
You will help the a team of organisers and provide them with a frontend for there 'organisers' backend. They should be able to:
* Create posters
* Create users, allocate poster to them
* Create sessions, allocate poster and speakers to session in a determined order

## Please note
* We implemented a REST api with support for HTTP HEAD, OPTIONS, GET POST and DELETE methods
* HTTP PUT is not supported
* Do not focus on the visual screen design, just concentrate on the functionality. 
* Do it simple and focus on the pipeline (dev > test > deploy)

# Screendesign description
## General use of InvisionApp Mockup
https://invis.io/YTCU73DXG leads you to the screen design mockup. The mockup contains “hotspot” which are clickable and lead you through the mockup. Holding shift on a page highlights the clickable area.

## Screen 1 – Sign Up Empty
https://projects.invisionapp.com/share/YTCU73DXG#/screens/246482700

This screen just lets the user create an organiser. We currently do NOT require a password. What happens if user already exists? You decide.

## Screen 2 – Sign Up Filled
https://projects.invisionapp.com/share/YTCU73DXG#/screens/246482701

Same as Screen 1, but with filled text fields

## Screen 3/4 - Manage posters
https://projects.invisionapp.com/share/YTCU73DXG#/screens/246482702
https://projects.invisionapp.com/share/YTCU73DXG#/screens/246482703

After having created the organiser, the user will be landing on his poster-overview. Here the user is able to create posters, authors and session. First the user will be creating a poster and author (which don’t exist separately) by clicking “Create Author + Poster”. The list of posters will be appearing where the user can enter Poster ID, email address of the author and title of the poster. 

## Screen 5 – Create Session
https://projects.invisionapp.com/share/YTCU73DXG#/screens/246482704

The user now can creates a session which is a sorted list of posters. Hitting “create session” will create a session. Each session can have a name and can contain multiple posters. 

## Screen 6 - Allocate posters to sessions
https://projects.invisionapp.com/share/YTCU73DXG#/screens/246482705

The user can create multiple posters incl. authors and multiple sessions. Posters can be allocated to sessions with drag and drop.

# What we expect
* test driven development if possible
* ReactJS
* pipeline: dev > test > deploy
* integrate build-tool of your choice
* provide information on how to build and test your application 

## Who do I talk to if something is unclear?
* Do not hesitate to contact justus.weweler@morressier.com for general questions regarding the challenge and the related tasks. 
* For technical questions, reach out to martin.kluth@morressier.com