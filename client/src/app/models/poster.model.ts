export class Poster {
    constructor(
        public id: string,
        public title: string,
        public author: string
    ) { }
}