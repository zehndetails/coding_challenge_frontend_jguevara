export class User {
    constructor (
        public id: string,
        public email: string,
        public title: string,
        public roles: string[],
        public posters: string[]
    ) {}
}