export class Session {
    constructor(
        public id: string,
        public title: string,
        public organiser: string,
        public slots: { order: string, poster: string, speaker: string}[]
    ) { }
}