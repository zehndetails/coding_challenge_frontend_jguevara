import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormControl, Validators, FormGroupDirective } from '@angular/forms';
import { Response } from '@angular/http';

import { User } from 'app/models/user.model';
import { Poster } from 'app/models/poster.model';
import { Session } from 'app/models/session.model';
import { UsersService } from 'app/services/users.service';
import { PostersService } from 'app/services/posters.service';
import { SessionsService } from 'app/services/sessions.service';

@Component({
  selector: 'app-posters',
  templateUrl: './posters.component.html',
  styleUrls: ['./posters.component.css']
})
export class PostersComponent implements OnInit {
  posterForm: FormGroup;
  sessionForm: FormGroup;
  users: { [id: string]: User } = {};
  userWithEnteredEmail: User = null;
  posters: Poster[] = [];
  postersById: { [id: string]: Poster } = {}; 
  sessions: Session[] = [];
  pageNumber: number = 0;
  pageSize: number = 10;
  pages: number[] = [];
  isAddingPoster: boolean = false;
  isAddingSession: boolean = false;

  constructor(private usersService: UsersService, private postersService: PostersService, private sessionsService: SessionsService) { }

  ngOnInit() {
    this.getUsers();
    this.getPosters();
    this.getSessions();
    this.posterForm = new FormGroup({
      "authorEmail": new FormControl(null, [Validators.required, Validators.email, this.validateAuthorWithEmailExist.bind(this)]),
      "title": new FormControl(null, Validators.required)
    });
    this.sessionForm = new FormGroup({
      "title": new FormControl(null, [Validators.required]),
      "organiser": new FormControl(this.usersService.currentUser().id),
      "slots": new FormArray([])
    });
  }

  getUsers() {
    this.usersService.getAll().subscribe(
      (response: Response) => {
        response.json().map((u) => {
          this.users[u.id] = u;
        });
      },
      (error: any) => { console.log(error); }
    );
  }

  getPosters() {
    this.pageNumber = 0;
    this.postersService.getAll().subscribe(
      (response: Response) => {
        this.posters = response.json().reverse();
        this.posters.map((p) => {
          this.postersById[p.id] = p;
        });
        this.pages = Array(Math.ceil(response.json().length / this.pageSize)).fill(0).map((x, i) => i + 1);
      },
      (error: any) => { console.log(error); }
    );
  }

  getSessions() {
    this.sessionsService.getAll().subscribe(
      (response: Response) => {
        this.sessions = response.json().reverse();
      },
      (error) => console.log(error)
    );
  }

  onSaveAuthorAndPoster() {
    this.markFormGroupTouched(this.posterForm);
    if (this.posterForm.valid) {
      this.postersService.create({ author: this.userWithEnteredEmail.id, title: this.posterForm.value.title }).subscribe(
        (response: Response) => { 
          this.getPosters(); 
          this.posterForm.reset();
        },
        (error) => { console.log(error); }
      );
    }
  }

  onSaveSession() {
    if(this.sessionForm.valid) {
      this.sessionsService.create(this.sessionForm.value).subscribe(
        (response: Response) => { 
          this.getSessions(); 
          this.sessionForm.reset();
        },
        (error) => { console.log(error); }
      );
    }
  }

  validateAuthorWithEmailExist(formControl: FormControl): { [s: string]: boolean } {
    this.userWithEnteredEmail = this.getUserByEmail(formControl.value);
    if (!this.userWithEnteredEmail)
      return { 'userWithEnteredEmailDoesNotExist': true };
    return null;
  }

  getUserByEmail(email: string): User {
    let user: User = null;
    Object.keys(this.users).forEach(key => {
      if (this.users[key].email === email)
        user = this.users[key];
    });
    return user;
  }

  private markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach(control => {
      control.markAsTouched();

      if (control.controls) {
        control.controls.forEach(c => this.markFormGroupTouched(c));
      }
    });
  }

  private onPreviousPage() {
    this.pageNumber = this.pageNumber - 1;
  }

  private onPageNumber(page) {
    this.pageNumber = page - 1;
  }

  private onNextPage() {
    this.pageNumber = this.pageNumber + 1;
  }

  onAddAuthorAndPoster() {
    this.isAddingPoster = !this.isAddingPoster;
    this.isAddingSession = false;
  }

  onAddSession() {
    this.isAddingSession = !this.isAddingSession;
    this.isAddingPoster = false;
  }

  addDropItem(event: Poster) {
    (<FormArray>this.sessionForm.get("slots")).push(new FormControl({
      "poster": event.id, 
      "speaker": event.author, 
      "order": (<FormArray>this.sessionForm.get("slots")).length + 1
    }));
  }

}
