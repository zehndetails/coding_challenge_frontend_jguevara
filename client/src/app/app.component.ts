import { Component } from '@angular/core';
import { UsersService } from 'app/services/users.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private usersService: UsersService, private router: Router) { }

  onSignOut() {
    this.usersService.setCurrent(null);
    this.router.navigate(['']);
  }
}
