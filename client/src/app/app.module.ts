import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { DragDropDirectiveModule} from "angular4-drag-drop";

import { AppComponent } from './app.component';
import { PostersComponent } from './posters/posters.component';
import { SignupComponent } from './signup/signup.component';
import { Routes, RouterModule } from '@angular/router';
import { UsersService } from 'app/services/users.service';
import { PostersService } from 'app/services/posters.service';
import { SessionsService } from 'app/services/sessions.service';

const appRoutes: Routes = [
  { path: '', component: SignupComponent },
  { path: 'posters', component: PostersComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    PostersComponent,
    SignupComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    DragDropDirectiveModule,
    [RouterModule.forRoot(appRoutes)]
  ],
  providers: [UsersService, PostersService, SessionsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
