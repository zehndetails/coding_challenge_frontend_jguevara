import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Response } from '@angular/http';
import { UsersService } from 'app/services/users.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(private usersService: UsersService, private router: Router) {}
  
  onSubmit(form: any) {
    this.usersService.createOrganiser(form.value).subscribe(
      (response) => {
        this.usersService.setCurrent(response.json());
        this.router.navigate(['posters']);
      }, 
      (error: Response) => {
        if(error.json().message === "User already existing") {
          //not much I can do here, there's no endpoint to get the user id by email only
        }
      }
    );
  }

  ngOnInit() {
  }

}
