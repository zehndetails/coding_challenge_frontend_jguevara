import { Injectable } from "@angular/core";
import { Http } from "@angular/http";

@Injectable()
export class SessionsService {
    private baseUrl: string = "https://mcc17.herokuapp.com/";
    
    constructor(private http: Http) { }

    create(session: {title: string, organiser: string, slots: any[]}) {
        return this.http.post(this.baseUrl + "sessions", session);
    }

    getAll() {
        return this.http.get(this.baseUrl + "sessions");
    }
}