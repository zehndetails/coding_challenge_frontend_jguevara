import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { User } from "app/models/user.model";

@Injectable()
export class UsersService {
    private baseUrl: string = "https://mcc17.herokuapp.com/";
    constructor(private http: Http) { }

    createOrganiser(user: {email: string, title: string, name: string, roles: string[]}) {
        user.roles = ["organiser"];
        return this.http.post(this.baseUrl + "users", user);
    }

    getAll() {
        return this.http.get(this.baseUrl + "users");
    }

    currentUser(): User {
        return JSON.parse(localStorage.getItem('morressierUser'));
    }

    setCurrent(user: User) {
        if(user)
            localStorage.setItem("morressierUser", JSON.stringify(user));
        else
            localStorage.removeItem('morressierUser');
    }

    isAuthenticated(): boolean {
        return localStorage.getItem('morressierUser') !== null;
    }
}