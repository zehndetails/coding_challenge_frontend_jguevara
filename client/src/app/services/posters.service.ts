import { Injectable } from "@angular/core";
import { Http } from "@angular/http";

@Injectable()
export class PostersService {
    private baseUrl: string = "https://mcc17.herokuapp.com/";
    
    constructor(private http: Http) { }
    
    create(poster: {author: string, title: string}) {
        return this.http.post(this.baseUrl + "posters", poster);
    }

    getAll() {
        return this.http.get(this.baseUrl + "posters");
    }
}